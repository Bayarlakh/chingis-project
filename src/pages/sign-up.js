import React, { useState, useContext } from 'react';
import { useHistory } from 'react-router-dom';
import { useFirebase } from '../Hooks/firebase';
import { AuthContext } from '../providers/auth-user-provider'
import { FormInput, ActionButton, Stack, Layout } from '../components'

export const SignUp = () => {
    const { user } = useContext(AuthContext)
    const [step, setStep] = useState(1);
    const [state, setState] = useState({ email: '', password: '', password2: '', username: '', phone: '', groupName: '', groupMail: '', medku: '' });
    const history = useHistory();
    const [error, setError] = useState('');
    const { firebase, auth, firestore } = useFirebase();

    const [tab, setTab] = useState('Individual');

    /////placeholder code
    setTab('Individual');
    /////////////////////
    
    const handleChangeUsername = (e) => setState({ ...state, username: e.target.value })
    const handleChangeEmail = (e) => setState({ ...state, email: e.target.value });
    const handleChangePassword = (e) => setState({ ...state, password: e.target.value });
    const handleChangePassword2 = (e) => setState({ ...state, password2: e.target.value });

    // const handleChangeGroupName = (e) => setState({ ...state, groupName: e.target.value });
    // const handleChangeGroupMail = (e) => setState({ ...state, groupMail: e.target.value });
    // const handleChangeMedku = (e) => setState({ ...state, medku: e.target.value });

    if (user) {
        history.push('/feed')
    }
    console.log(error);

    // const facebook = () => {
    //     var provider = new firebase.auth.FacebookAuthProvider();

    //     auth.signInWithPopup(provider).then(function (result) {
    //         var token = result.credential.accessToken;
    //         var user = result.user;
    //         console.log(token);
    //         console.log(user);
    //         console.log(user.emailVerified)

    //         firestore.collection('users').doc(user.uid).set({
    //             username: user.displayName,
    //             createdAt: firebase.firestore.FieldValue.serverTimestamp(),
    //             logged: "Facebook",
    //             profileImage: "default",
    //             uid: user.uid
    //         });
    //         history.push('/feed')

    //     }).catch(function (error) {
    //         var errorMessage = error.message;
    //         alert(errorMessage)
    //     });
    // }

    const google = () => {
        var provider = new firebase.auth.GoogleAuthProvider();

        auth.signInWithPopup(provider).then((result) => {
            var token = result.credential.accessToken;
            var user = result.user;

            console.log(token);
            console.log(user);

            console.log(user.emailVerified)

            firestore.collection('users').doc(user.uid).set({
                username: user.displayName,
                createdAt: firebase.firestore.FieldValue.serverTimestamp(),
                logged: "Google",
                profileImage: "default",
                uid: user.uid
            });

            if (user.emailVerified === true) {
                history.push('/feed')
            } else {
                history.push('/Verify')
            }
        }).catch(function (error) {
            var errorMessage = error.message;

            console.log(errorMessage)
        });
    }

    const isFilled = () => {
        if (state.username === '')
            return true;
        if (state.email === '')
            return true;
        if (state.password === '')
            return true;
        if (state.password2 === '')
            return true;
        return false
    }


    const signUp = async () => {
        if (tab !== 'Group') {
            if (!(state.email && state.password && state.password2)) {
                setError('Please enter all the required information');
                return;
            }
            if (state.password !== state.password2) {
                setError('Passwords dont match!');
                return;
            }
            let cred = await auth.createUserWithEmailAndPassword(state.email, state.password)
                .catch((error) => {
                    setError(error.message)
                })
            
            if (cred) {
                await firestore.collection('users').doc(cred.user.uid).set({
                    username: state.username,
                    createdAt: firebase.firestore.FieldValue.serverTimestamp(),
                    logged: "default",
                    profileImage: "default",
                    uid: cred.user.uid
                });

                history.push('/Verify')
            }
        } else {
            if (!(state.groupName && state.groupMail && state.medku)) {
                setError('Please enter all the required information');
                return;
            }
            let group = await auth.createUserWithEmailAndPassword(state.groupMail, state.medku)
                .catch((error) => {
                    alert(error.message)
                })

            await firestore.collection('users').doc(group.user.uid).set({
                username: state.groupName,
                createdAt: firebase.firestore.FieldValue.serverTimestamp(),
                logged: "Group",
                profileImage: "default",
                uid: group.user.uid
            });

        }

    }

    return (
        <Layout>
            <div className="font-main w90 text-center font-Raleway">
                <div className="flex-col" style={{height: 'calc(100vh - 100px)'}}>
                    <div className="mb-150">
                        <div className="font-main bold fs-24">Бүртгүүлэх </div>
                        <div className="c-gray3 font-Raleway fs-16 normal mt-24">Та өөрийн бүртгэлээ үүсгэнэ үү? </div>
                    </div>

                    {step === 1 && 
                        <Stack size={4}>
                            <ActionButton className="font-main mt-6 h-50 b-gmail-color c-primaryfourth brad-5 pa-12" icon='gmail' onClick={google}>Google хаягаар бүртгүүлэх</ActionButton>
                            <ActionButton className="font-main h-50 c-primary b-white brad-5 boxshadow" icon="white" onClick={() => {setStep(2)}}>Емайл хаягаар нэвтрэх</ActionButton>
                            <div className="flex justify-center">
                                <div>Бүртгэл байхгүй ?</div>
                                <div className="ml-5 text-button c-primary bold">Бүртгүүлэх </div>
                                <positivityIcon height={110} width={183}></positivityIcon>
                            </div>
                        </Stack>
                    }

                    {step === 2 && 
                        <>
                            <Stack size={4}>
                                <FormInput className="bradius-5 ph-44 h-50 br-border-color-1" label='Хэрэглэгчийн нэр' type='text' icon='user' placeholder='Username' value={state.username} onChange={handleChangeUsername} />
                                <FormInput className="bradius-5 ph-44 h-50 br-border-color-1" label='Цахим хаяг' placeholder='name@mail.com' type='email' value={state.email} onChange={handleChangeEmail} />
                                <FormInput className="bradius-5 ph-44 h-50 br-border-color-1" label='Нууц үг' placeholder='Password' type='password' value={state.password} onChange={handleChangePassword} />
                                <FormInput className="bradius-5 ph-44 h-50 br-border-color-1" label='Нууц үгээ давтна уу?' placeholder='Password' type='password' value={state.password2} onChange={handleChangePassword2} />
                                <ActionButton className="font-main h-50 c-primaryfourth b-primary brad-5" disabled={isFilled()} icon="signupin" onClick={signUp}>Бүртгүүлэх</ActionButton>


                                <div className="flex justify-center">
                                    <div>Нууц үг мартсан?</div>
                                    <div className="ml-5 text-button c-primary bold">Энд дарна уу</div>
                                </div>
                            </Stack>


                            
                            <div className="flex-col">
                                <div className="mb-24 c-gray5">Эсвэл үүгээр бүртгүүлж болно</div>
                                {/* <ActionButton className="h-50 b-fb-color c-primaryfourth brad-5 pa-12" icon='facebook' onClick={facebook}>Facebook</ActionButton>       */}
                                <ActionButton className="font-main mt-6 h-50 b-gmail-color c-primaryfourth brad-5 pa-12" icon='gmail' onClick={google}>Google хаягаар бүртгүүлэх</ActionButton>
                            </div>
                        </>
                    }
                </div>
            </div>
        </Layout>
    )
}
