import React, { useContext, useState } from 'react';
import { useHistory } from 'react-router-dom'
import { AuthContext } from '../providers/auth-user-provider';
import { useFirebase } from '../Hooks/firebase';
import { Layout, FormInput, ActionButton, Stack, positivityIcon} from '../components'

export const SignIn = () => {
    const history = useHistory();
    const { user } = useContext(AuthContext);
    const { auth, firebase } = useFirebase();
    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');
    const [showLoginForm, setShowLoginForm] = useState(false);

    // const { user } = useContext(AuthContext)
    // const [state, setState] = useState({ email: '', password: '', password2: '', username: '', phone: '', groupName: '', groupMail: '', medku: '' });
    // const history = useHistory();
    // const [error, setError] = useState('');
    // const { firebase, auth, firestore } = useFirebase();

    // const signUpPage = () => {
    //     history.push('./register')
    // }
    console.log(user);

    if (user) {
        history.push('/feed')
    }

    const handleChangeEmail = (e) => setEmail(e.target.value);
    const handleChangePassword = (e) => setPassword(e.target.value);

    // const handleChangeEmail = (e) => setState({ ...state, email: e.target.value });
    // const handleChangePassword = (e) => setState({ ...state, password: e.target.value });


    const signIn = async () => {
        await auth.signInWithEmailAndPassword(email, password).catch((error) => {
            alert(error.message);
        })
    }

    // const facebook = () => {
    //     var provider = new firebase.auth.FacebookAuthProvider();
    
    //     auth.signInWithPopup(provider).then(function(result) {
    //         var token = result.credential.accessToken;
    //         var user = result.user;
    //         console.log(token);
    //         console.log(user);
    //     }).catch(function(error) {
    //         var errorMessage = error.message;
    //         alert(errorMessage)
    //     });
    // }

    const google = () => {
        var provider = new firebase.auth.GoogleAuthProvider();


        auth.signInWithPopup(provider).then((result) => {
            var token = result.credential.accessToken;
            var user = result.user;

            console.log(token);
            console.log(user);

          }).catch(function(error) {
            var errorMessage = error.message;

            console.log(errorMessage)
          });
    }

    const isFilled = () => {
       
        if (email === '')
            return true;
        if (password === '')
            return true;
      
        return false
    }


    return (
        // <Layout>
        //     <div className='items-center'>
        //         <div>
        //             <p className='title'>Нэвтрэх</p>
        //             <FormInput label='Цахим хаяг' placeholder='name@mail.domain' value={email} onChange={handleChangeEmail} />
        //             <div className='mt-4'></div>
        //             <FormInput label='Нууц үг' type='password' placeholder='Password' value={password} onChange={handleChangePassword} />
        //             <ActionButton onClick={signIn}>Нэвтрэх</ActionButton>
        //         </div>
        //         <div className='mt-30'>
        //             <ActionButton onClick={facebook}>Facebook</ActionButton>
        //             <ActionButton onClick={google}>Google</ActionButton>
        //         </div>
        //     </div>
        // </Layout>
        <Layout>
        <div className="font-main w90 text-center font-Raleway">
            <div className="flex-col justify-between" style={{height: 'calc(100vh - 229px)'}}>
                <div className="">
                    <div class="font-main bold fs-24">Нэвтрэх </div>
                    <div className="c-gray3 font-Raleway fs-16 normal mt-24">Та өөрийн бүртгэлээрээ нэвтэрнэ үү?  </div>
                    
                </div>

                {!showLoginForm && 
                    <Stack size={4}>
                        <ActionButton className="font-main mt-6 h-50 b-gmail-color c-primaryfourth brad-5 pa-12" icon='gmail' onClick={google}>Google хаягаар бүртгүүлэх</ActionButton>
                        <ActionButton className="font-main h-50 c-primary b-white brad-5 boxshadow" icon="white" onClick={() => {setShowLoginForm(true)}}>Емайл хаягаар нэвтрэх</ActionButton>
                        <div className="flex justify-center">
                            <div>Бүртгэл байхгүй ?</div>
                            <div className="ml-5 text-button c-primary bold">Бүртгүүлэх </div>
                            <positivityIcon height={110} width={183}></positivityIcon>
                        </div>
                    </Stack>
                    
                }

                {showLoginForm && 
                <Stack size={4}>
                    
                    <FormInput className="bradius-5 ph-44 h-50 br-border-color-1" label='Цахим хаяг' placeholder='name@mail.com' type='email' value={email} onChange={handleChangeEmail} />
                    <FormInput className="bradius-5 ph-44 h-50 br-border-color-1" label='Нууц үг' placeholder='Password' type='password' value={password} onChange={handleChangePassword} />
                    <ActionButton className="font-main h-50 c-primaryfourth b-primary brad-5" disabled={isFilled()} icon="signupin" onClick={signIn}>Бүртгүүлэх</ActionButton>


                <div className="flex justify-center">
                    <div className='font-main'>Бүртгэл байхгүй ?</div>
                    <div className="ml-5 text-button c-primary bold">Бүртгүүлэх</div>
                </div>
                </Stack>}
                
                <div className="flex-col">
                </div>


            </div>
        </div>
    </Layout>
    )
}

  

                    
