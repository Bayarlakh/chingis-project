import React, { useState } from 'react';
// import { AuthContext } from '../providers/auth-user-provider'
import { Stack, SearchInput} from '../components'
import { XIcon } from '../components/icons/x-icon'
import { VerifiedIcon } from '../components/icons/verified-icon'
import {TenderCampaign2 } from '../components/tender-accounts'
import { useCol } from '../Hooks';
// import { useHistory } from 'react-router-dom'

export const Adminz = () => {
    // const history = useHistory();
    // const { user } = useContext(AuthContext);
    const [flag, setFlag] = useState(false);
    const { data: userInfos, updateRecord } = useCol(`users/`)

    // let uid;

    // if (user != null) {
    //     uid = user.uid
    // }

    // const { data } = useDoc(`users/${uid}`);
    const toggle = () => setFlag((flag) => !flag);

    // if(data && !data.adminz) {
    //     history.push('/feed')
    // } 

    return (
        <div className="h-vh-100">
            <Stack>
                <div className="mt-30 mb-10 flex-center">
                    <div className="lh-31 fw700 fs-24 font-main">Verify Account</div>
                </div>
                <SearchInput placeholder="Verified Account" />
                <TenderCampaign2 toggle={toggle} />
                    {
                        flag ?
                            userInfos && userInfos.map((e) => {
                                if(e.logged === 'Group') {
                                    if(e.verified !== true) {
                                        return (
                                            <div className="flex-row bb-gray2-1 pa-10 justify-between">
                                                <div className="z-2 mt-8 searchtext fw-400 font-main h-35 fs-16 lh-20">{e.username}</div>
                                                <div className='flex items-center w-50 justify-between'>
                                                    <VerifiedIcon onClick={() => {updateRecord(e.uid, {verified: true})}} width={25} height={25}/>
                                                    <XIcon width={16} height={16}/>
                                                </div>
                                            </div>
                                        )
                                    }
                                }
                                return ''
                            })
                        :
                            userInfos && userInfos.map((e) => {
                                if(e.logged === 'Group') {
                                    if(e.verified === true) {
                                        return (
                                            <div className="flex-row bb-gray2-1 pa-10 justify-between">
                                                <div className="z-2 mt-8 searchtext fw-400 font-main h-35 fs-16 lh-20">{e.username}</div>
                                                <div className='flex items-center w-50 justify-between'>
                                                    <VerifiedIcon onClick={() => {updateRecord(e.uid, {verified: true})}} width={25} height={25}/>
                                                    <XIcon onClick={() => {updateRecord(e.uid, {verified: null})}} width={16} height={16}/>
                                                </div>
                                            </div>
                                        )
                                    }
                                }
                                return ''
                            })
                    }

            </Stack>
        </div>
    )
}