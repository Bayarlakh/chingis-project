import React from 'react';
import { Stack, SearchInput, FormInput } from '../components'

export const Components = () => {
    return (
        <div className='container'>
            <Stack size={10}>
                <SearchInput />
                <SearchInput placeholder="search..."/>
                <div className="pa-0">
                <FormInput placeholder="enter text"/>
                <FormInput placeholder="enter password" type="password"/>
                </div>
            </Stack>
        </div>
    )
}