import React, { useContext, useState } from 'react';
import { useHistory } from 'react-router-dom'
import { AuthContext } from '../providers/auth-user-provider';
import { FormInput, ActionButton, Layout } from '../components/index'
import { useFirebase } from '../Hooks';

export const ForgotPass = () => {
    const [email, setEmail] = useState('');
    const { auth } = useFirebase();
    const history = useHistory();
    const { user } = useContext(AuthContext);

    const Send = () => {
        auth.sendPasswordResetEmail(email).then(() => {
            // Email sent.
            console.log("Sent")
        }).catch(function (error) {
            console.log(error)
        });
    }


    console.log(user);

    if (user) {
        history.push('/feed')
    }

    const handleChangeEmail = (e) => setEmail(e.target.value);

    return (
        <Layout>
            <div>
                <div>
                    <div className='flex-center'>
                        <p className='title'>Нууц үгээ мартсан?</p>
                        <FormInput className='w-vw-87 brad-5 h-50 pl-20 bb-lightgray-1 boxs' label='Цахим хаяг' placeholder='name@mail.domain' value={email} onChange={handleChangeEmail} />
                        <div className='mt-4'></div>
                        {
                            email === '' ?
                                <ActionButton disabled >Илгээх</ActionButton>
                                :
                                <ActionButton onClick={Send} >Илгээх</ActionButton>
                        }
                    </div>
                </div>
            </div>
        </Layout>
    )
}
