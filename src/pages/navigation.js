import React from 'react'
import { DropdownMenu, ArrowIcon, Grid } from '../components/';
import { useHistory } from 'react-router-dom';
import _ from 'lodash';

export const Navigation = ({ children }) => {

    let history = useHistory();

    return (
        <Grid className='h-70 primary-gradient w100' columns="3">
            <div className='flex items-center justify-start'>
                {
                    !_.isEmpty(
                        _.chain([
                            '/feed',
                        ])
                            .filter(path => path === '/feed' ? history.location.pathname === '/feed' : history.location.pathname.match(path))
                            .value()
                    )
                    && <h3 className="font-glegoo ml-10 fs-24 lh-43 c-white" onClick={() => { history.push("/feed") }}>HHT</h3>
                }

                {
                    !_.isEmpty(
                        _.chain([
                            '/event-id',
                            '/search',
                            '/tender-id',
                            "/verify-account",
                            "/profile",
                            '/vote-history',
                        ])
                            .filter(path => path === '/feed' ? history.location.pathname === '/feed' : history.location.pathname.match(path))
                            .value()
                    )
                    && <ArrowIcon className="ml-20" onClick={() => { history.goBack() }} width={20} height={20} />
                }

            </div>
            <div className='flex items-center justify-center'>
                {
                    !_.isEmpty(
                        _.chain([
                            '/profile',
                        ])
                            .filter(path => path === '/feed' ? history.location.pathname === '/feed' : history.location.pathname.match(path))
                            .value()
                    )
                    && <div className='items-center flex ml-10'>
                        <span className='font-main bold fs-20'>Profile</span>
                    </div>
                }
                {
                    !_.isEmpty(
                        _.chain([
                            '/vote-history',
                        ])
                            .filter(path => path === '/feed' ? history.location.pathname === '/feed' : history.location.pathname.match(path))
                            .value()
                    )
                    && <div className='items-center flex'>
                        <span className='font-main bold fs-20'>History</span>
                    </div>
                }
            </div>
            <div className='flex items-center justify-end'>
                {
                    !_.isEmpty(
                        _.chain([
                            '/feed',
                            '/event-id',
                            '/search',
                            '/tender-id',
                            '/profile'
                        ])
                            .filter(path => path === '/feed' ? history.location.pathname === '/feed' : history.location.pathname.match(path))
                            .value()
                    )
                    && <DropdownMenu ></DropdownMenu>
                }

                {
                    !_.isEmpty(
                        _.chain([
                            '/register'
                        ])
                            .filter(path => path === '/feed' ? history.location.pathname === '/feed' : history.location.pathname.match(path))
                            .value()
                    )
                    && <p>Algasah</p>
                }

            </div>
        </Grid>
    )
}