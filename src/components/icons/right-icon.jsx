import React from 'react'

export const RightIcon = ({ height, width, color = "#999999", ...others }) => {
    return (
        <span {...others} >
            <svg width={width} height={height} viewBox="0 0 8 13" fill="none" xmlns="http://www.w3.org/2000/svg">
                <path d="M1 12.5L7 6.5L1 0.5" stroke={color} strokeLinecap="round" strokeLinejoin="round" />
            </svg>
        </span>
    )
}