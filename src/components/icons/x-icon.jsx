import React from 'react'

export const XIcon = ({ height, width, color = "#52575C", ...others }) => {
    return (
        <span {...others}>
            <svg width={width} height={height} viewBox="0 0 14 14" fill="none" xmlns="http://www.w3.org/2000/svg">
                <path d="M13 1L1 13M1 1L13 13" stroke="#52575C" stroke-linecap="round" stroke-linejoin="round" />
            </svg>

        </span>
    )
}