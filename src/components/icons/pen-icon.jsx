import React from 'react'

export const PenIcon = ({ height, width, color = "#52575C", ...others }) => {
    return (
        <span {...others}>
            <svg width={width} height={height} viewBox="0 0 19 19" fill="none" xmlns="http://www.w3.org/2000/svg">
                <path d="M13.071 1.46423L17.5353 5.92852L5.92815 17.5357H1.46387V13.0714L13.071 1.46423Z" stroke={color} strokeLinecap="round" strokeLinejoin="round" />
            </svg>
        </span>
    )
}