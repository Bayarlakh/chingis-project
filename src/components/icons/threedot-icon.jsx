import React from 'react'

export const DotIcon = ({ height, width, color = "#52575C", ...others }) => {
    return (
        <span {...others}>
            <svg width={width} height={height} viewBox="0 0 17 3" fill="none" xmlns="http://www.w3.org/2000/svg">
                <path d="M8.50028 2.39282C8.99339 2.39282 9.39314 1.99308 9.39314 1.49997C9.39314 1.00685 8.99339 0.607108 8.50028 0.607108C8.00717 0.607108 7.60742 1.00685 7.60742 1.49997C7.60742 1.99308 8.00717 2.39282 8.50028 2.39282Z" stroke={color} strokeLinecap="round" strokeLinejoin="round" />
                <path d="M14.7503 2.39282C15.2434 2.39282 15.6431 1.99308 15.6431 1.49997C15.6431 1.00685 15.2434 0.607108 14.7503 0.607108C14.2572 0.607108 13.8574 1.00685 13.8574 1.49997C13.8574 1.99308 14.2572 2.39282 14.7503 2.39282Z" stroke={color} strokeLinecap="round" strokeLinejoin="round" />
                <path d="M2.25028 2.39282C2.74339 2.39282 3.14314 1.99308 3.14314 1.49997C3.14314 1.00685 2.74339 0.607108 2.25028 0.607108C1.75717 0.607108 1.35742 1.00685 1.35742 1.49997C1.35742 1.99308 1.75717 2.39282 2.25028 2.39282Z" stroke={color} strokeLinecap="round" strokeLinejoin="round" />
            </svg>
        </span>
    )
}
