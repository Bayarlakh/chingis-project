import React from 'react'

export const Loader = () => {
    return (
        <div className='flex-center h-vh-80 bold'>
            <h3>Loading ...</h3>
            <div className="loader"></div>
        </div>
    )
}